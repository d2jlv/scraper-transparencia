var connection = require('./mysqlDriver').MySqlConnection;

var trace = true;

var dataAccessObject = {}

dataAccessObject.connect = () => {
    return new Promise((resolve, reject) => {
        connection.connect(function (err) {
            if (err)
                return reject(err);

            if (trace)
                console.log(['connected']);

            resolve();
        });
    });
}

dataAccessObject.endConnection = () => {
    connection.end(function (err) {
        if (err)
            return console.log(err);

        if (trace)
            console.log(['connection ended']);
    });
}

dataAccessObject.beginTran = () => {
    return new Promise((resolve, reject) => {
        var sql = 'START TRANSACTION';
        connection.query(sql, function (err) {
            if (err)
                return reject(err);

            if (trace)
                console.log(['command', sql]);

            resolve();
        });
    });
}

dataAccessObject.commit = () => {
    return new Promise((resolve, reject) => {
        var sql = 'COMMIT';
        connection.query(sql, function (err) {
            if (err)
                return reject(err);

            if (trace)
                console.log(['command', sql]);

            resolve();
        });
    });
}

dataAccessObject.rollback = () => {
    return new Promise((resolve, reject) => {
        var sql = 'ROLLBACK';
        connection.query(sql, function (err) {
            if (err)
                return reject(err);

            if (trace)
                console.log(['command', sql]);

            resolve();
        });
    });
}

dataAccessObject.getConnection = () => { return connection; }

module.exports.DataAccessObject = dataAccessObject;