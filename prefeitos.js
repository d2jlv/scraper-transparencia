function Prefeitos(dataAccessObject) {

    var trace = true;

    this.findByYear = (year) => {
        if (year > 2004 && year < 2009) return 'Junji Abe';
        if (year > 2008 && year < 2017) return 'Marco Aurelio Bertaiolli';
        if (year > 2016 && year < 2021) return 'Marcus Vinicius de Almeida e Melo';

        return 'PREFEITO';
    }

    this.seed = () => {
        return new Promise((resolve, reject) => {
            var sql = 'INSERT INTO autor (idSiteProjeto, nome, apelido, partido, atividade, formacao, dataNascimento, estadoCivil) VALUES ?';
            var values = [];
            values.push(['X', 'Junji Abe', 'Junji Abe', 'PSDB', 'Empresário', 'Superior Incompleto', '1940-12-15', 'Casado']);
            values.push(['Y', 'Marco Aurelio Bertaiolli', 'Marco Bertaiolli', 'DEMOCRATAS', 'Administrador', 'Superior Completo', '1968-04-30', 'Casado']);
            values.push(['Z', 'Marcus Vinicius de Almeida e Melo', 'Marcus Melo', 'PSDB', 'Administrador', 'MBA', '1972-09-01', 'Casado']);
            dataAccessObject.getConnection().query(
                sql,
                [values],
                function (err, results) {
                    if (err)
                        return reject(err);

                    if (trace)
                        console.log(['command', sql, 'affected rows', results.affectedRows]);

                    resolve();
                });
        })
    }
}

module.exports.Prefeitos = Prefeitos;