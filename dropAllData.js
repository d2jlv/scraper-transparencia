var trace = true;

var drop = {
    foreingKeyIO: (dataAccessObject, enableKey) => {
        return new Promise((resolve, reject) => {
            var sql = 'SET FOREIGN_KEY_CHECKS=';
            if (enableKey)
                sql += '1'
            else sql += '0';

            dataAccessObject.getConnection().query(sql, function (err) {
                if (err)
                    return reject(err);

                if (trace)
                    console.log(['command', sql]);

                resolve();
            });
        });
    },

    deleteFrom: (dataAccessObject, table) => {
        return new Promise((resolve, reject) => {
            var sql = 'DELETE FROM ' + table;
            dataAccessObject.getConnection().query(sql, function (err, results) {
                if (err)
                    return reject(err);

                if (trace)
                    console.log(['command', sql, 'affected rows', results.affectedRows]);

                resolve();
            });
        });
    }
}

module.exports.dropAllData = drop;