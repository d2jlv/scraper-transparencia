var mysql = require('mysql');

var connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'root',
    database: 'grupo_e',
    insecureAuth: true,
    multipleStatements: true //
});

var formatQuery = (sql, values) => {
    return mysql.format(sql, values);
}

module.exports.MySqlConnection = connection;
module.exports.QueryFormartter = formatQuery;