var mysql = require('mysql');

var connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'root',
    database: 'grupo_e',
    insecureAuth: true
});

module.exports = connection;