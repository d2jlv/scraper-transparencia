function buildDetalhes(identificadoresExternosItem, DOM) {

    var authorDetails = []

    var formulario = DOM('table[bgcolor="#FBFBFB"]').find('p[class="style76"]');
    var hasEstadoCivil = formulario.text().includes('ESTADO CIVIL:'); //contornar a ausência do campo ESTADO CIVIL de alguns autores

    authorDetails.push(extractPartido(DOM));
    authorDetails.push(extractor.extractStringBetween(formulario.text(), 'ATIVIDADE:', 'FORMAÇÃO:'));
    authorDetails.push(extractor.extractStringBetween(formulario.text(), 'FORMAÇÃO:', 'DATA DE NASCIMENTO:'));
    authorDetails.push(extractDataNascimento(formulario, hasEstadoCivil));
    authorDetails.push(hasEstadoCivil ? extractor.extractStringBetween(formulario.text(), 'ESTADO CIVIL:', 'ELEITO COM:') : null);
    authorDetails.push(extractVotos(formulario));
    authorDetails.push(extractor.extractStringAfter(formulario.text(), 'LEGISLATURA:'));
    authorDetails.push(extractEmail(DOM));
    authorDetails.push(identificadoresExternosItem);

    return authorDetails;
}

function extractPartido(DOM) {
    var partidoAsImage = DOM('img[width="60"][height="64"]').attr('src');
    if (partidoAsImage == '../siteadmin/partidos/logo/') //imagem do partido indisponível
        return null;

    return partidoAsImage.substring(partidoAsImage.lastIndexOf('/') + 1, partidoAsImage.indexOf('.jpg'))
        .toUpperCase();
}

function extractDataNascimento(formulario, hasEstadoCivil) {
    var dataNascimentoAsText = hasEstadoCivil ? extractor.extractStringBetween(formulario.text(), 'DATA DE NASCIMENTO:', 'ESTADO CIVIL:') :
        extractor.extractStringBetween(formulario.text(), 'DATA DE NASCIMENTO:', 'ELEITO COM:');

    var dataNascimento = dataNascimentoAsText == '00/00/0000' ? null :
        new Date(dataNascimentoAsText.split('/')[2], dataNascimentoAsText.split('/')[1] - 1, dataNascimentoAsText.split('/')[0]);

    return dataNascimento ? dataNascimento.getFullYear() + '-' + (dataNascimento.getMonth() + 1) + '-' + dataNascimento.getDate() :
        dataNascimento;
}

function extractVotos(formulario) {
    var votosAsText = extractor.extractStringBetween(formulario.text(), 'ELEITO COM:', 'LEGISLATURA:')
        .replace('votos', '')
        .replace('.', '');

    return votosAsText == '' ? null :
        votosAsText;
}

function extractEmail(DOM) {
    var email = DOM('a[href*="mailto"]').text().trim();

    if (email.split('/').length > 1)
        return email.split('/')[0].trim();

    if (email.split(' ').length > 1)
        return email.split(' ')[0].trim();

    return email;
}

var extractor = {
    extractStringBetween: (content, after, before) => {
        return content.substring(content.indexOf(after) + after.length, content.indexOf(before))
            .trim();
    },
    extractStringAfter: (content, after) => {
        return content.substring(content.indexOf(after) + after.length, content.length - 1)
            .trim();
    }
}

module.exports.builderDetalhes = buildDetalhes;

