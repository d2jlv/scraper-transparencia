var dataAccessObject = require('./dataAccess').DataAccessObject;
var Autores = require('./autores2').Autores;
var Prefeitos = require('./prefeitos').Prefeitos;
var Projetos = require('./projetos2').Projetos;

//main
(async function () {

    var autores = new Autores(dataAccessObject);
    var prefeitos = new Prefeitos(dataAccessObject);
    var projetos = new Projetos(dataAccessObject, prefeitos);

    //set connection & begin tran
    dataAccessObject.connect()
        .then(dataAccessObject.beginTran)

        //autores
        .then(autores.readAutores)
        .then(autores.dropAllData)
        .then(autores.insertAutores)
        .then(prefeitos.seed)
        .then(autores.readIdentificadoresExternos)
        .then(autores.readAuthorDetails)
        .then(autores.updateAuthorsDetails)
        .then(autores.readNomeFantasia)
        .then(autores.updateAuthorsNomeFantasia)

        //projetos
        .then(projetos.readProjects)
        .then(projetos.mapProjectAuthorRelationship)
        .then(projetos.dropAllData)
        .then(projetos.insertAllProjects)
        .then(projetos.insertAllmapProjectAuthorRelationship)

        //commit
        .then(dataAccessObject.commit)
        .then(dataAccessObject.endConnection)

        //error handler
        .catch((err) => {
            console.log(err);
            dataAccessObject.rollback()
                .then(dataAccessObject.endConnection);
        });

})();