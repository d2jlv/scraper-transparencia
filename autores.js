var request = require('request-promise');
var cheerio = require('cheerio');
var formatQuery = require('./mysqlDriver').QueryFormartter;
var dataAccessObject = require('./dataAccess').DataAccessObject;
var prefeitos = require('./prefeitos').Prefeitos;
var buildDetalhes = require('./autoresDetalhes').builderDetalhes;
var dropAllData = require('./dropAllData').dropAllData;

var trace = true;

var authors = [];
async function readAutores() {
    var response = await request({ uri: 'http://cmmc.com.br/projetos/plo.php?pg=0', encoding: 'latin1' });
    var $ = cheerio.load(response);

    var comboBoxAutores = $('select[name=textfield_autor]');
    var optionsAutores = comboBoxAutores.children();

    for (let index = 1; index < optionsAutores.length; index++) {
        let autoresArray = [];
        autoresArray.push(
            $(optionsAutores[index]).attr('value'),
            $(optionsAutores[index]).text(),
            $(optionsAutores[index]).text()
        );
        authors.push(autoresArray);
    }

    if (trace)
        console.log(['authors length', authors.length]);
}

function insertAutores() {
    return new Promise((resolve, reject) => {
        var sql = 'INSERT INTO autor (idSiteProjeto, nome, apelido) VALUES ?';
        dataAccessObject.getConnection().query(sql, [authors], function (err, results) {
            if (err)
                return reject(err);

            if (trace)
                console.log(['command', sql, 'affected rows', results.affectedRows]);

            resolve();
        });
    });
}

var identificadoresExternos = []
function readIdentificadoresExternos() {
    return new Promise((resolve, reject) => {
        var sql = 'SELECT * FROM autor WHERE idSiteProjeto NOT IN (?)';
        var excludedIdentificadoresExternos = ['X', 'Y', 'Z', 'P', 'I', 'M', 'C'];
        dataAccessObject.getConnection().query(sql, [excludedIdentificadoresExternos], function (err, results) {
            if (err)
                return reject(err);

            results.forEach(resultsItem => {
                identificadoresExternos.push(resultsItem.idSiteProjeto);
            });

            if (trace)
                console.log(['command', sql, 'excludedIdentificadoresExternos', excludedIdentificadoresExternos, 'rows returned', results.length]);

            resolve();
        });
    });
}

var authorsDetails = [];
async function readAuthorDetails() {
    var pageUrl = 'http://www.cmmc.com.br/vereadores/exibe_vereador.php?codigo=';

    for (let index = 0; index < identificadoresExternos.length; index++) {
        var identificadoresExternosItem = identificadoresExternos[index];
        var responseFormulario = await request({ uri: pageUrl + identificadoresExternosItem, encoding: 'latin1' });
        var $ = cheerio.load(responseFormulario);
        authorsDetails.push(buildDetalhes(identificadoresExternosItem, $));
    }

    if (trace)
        console.log(['authorsDetails length', authorsDetails.length]);
}

function updateAuthorsDetails() {
    return new Promise((resolve, reject) => {
        var queries = '';
        authorsDetails.forEach(function (authorDetail) {
            queries += formatQuery("UPDATE autor SET partido = ?, atividade = ?, formacao = ?, dataNascimento = ?, estadoCivil = ?, votosRecebidos = ?, legislatura = ?, email = ? WHERE idSiteProjeto = ?;", authorDetail);
        });

        dataAccessObject.getConnection().query(queries, function (err) {
            if (err)
                return reject(err);

            if (trace)
                console.log(['update AuthorsDetails', 'affected rows', authorsDetails.length]);

            resolve();
        });
    });
}

var authorsNomeFantasia = [];
async function readNomeFantasia() {
    var response = await request({ uri: 'http://cmmc.com.br/portalcidadao/', encoding: 'latin1' });
    var $ = cheerio.load(response);

    var painel = $('table[bordercolor="#F3F3F3"]');

    for (let index = 0; index < authorsDetails.length; index++) {
        var email = authorsDetails[index][7]; //posição do email no array de detalhes do autor
        if (email && painel.text().includes(email)) {
            var searchExpression = 'a[href="mailto:' + email + '"]';
            var emailSibilings = $(searchExpression).siblings();
            if (emailSibilings.length > 0) {
                var authorNomeFantasia = [];
                authorNomeFantasia.push(emailSibilings.first().text());
                authorNomeFantasia.push(authorsDetails[index][8]); //posição do id no array de detalhes do autor

                authorsNomeFantasia.push(authorNomeFantasia);
            }
        }
    }
}

function updateAuthorsNomeFantasia() {
    return new Promise((resolve, reject) => {
        var queries = '';
        authorsNomeFantasia.forEach(function (authorNomeFantasia) {
            queries += formatQuery("UPDATE autor SET apelido = ? WHERE idSiteProjeto = ?;", authorNomeFantasia);
        });

        dataAccessObject.getConnection().query(queries, function (err) {
            if (err)
                return reject(err);

            if (trace)
                console.log(['update AuthorsNomeFantasia', 'affected rows', authorsNomeFantasia.length]);

            resolve();
        });
    });
}

//main
(async function () {
    readAutores()
        .then(dataAccessObject.connect)
        .then(dataAccessObject.beginTran)
        .then(() => { return dropAllData.foreingKeyIO(dataAccessObject, false); })
        .then(() => { return dropAllData.deleteFrom(dataAccessObject, 'autor'); })
        .then(() => { return dropAllData.foreingKeyIO(dataAccessObject, true); })
        .then(insertAutores)
        .then(() => { return prefeitos.seed(dataAccessObject.getConnection()); })
        .then(readIdentificadoresExternos)
        .then(readAuthorDetails)
        .then(updateAuthorsDetails)
        .then(readNomeFantasia)
        .then(updateAuthorsNomeFantasia)
        .then(dataAccessObject.commit)
        .then(dataAccessObject.endConnection)
        .catch((err) => {
            console.log(err);
            dataAccessObject.rollback().then(dataAccessObject.endConnection);
        });
})();

