var request = require('request-promise');
var cheerio = require('cheerio');
var dataAccessObject = require('./dataAccess').DataAccessObject;
var prefeitos = require('./prefeitos').Prefeitos;
var dropAllData = require('./dropAllData').dropAllData;

var trace = true;

var projectTableName = 'projeto_lei';
var relationshipTableName = 'projeto_lei_x_autor';
var intialYear = '2019'; //2005
var finalYear = '2020';

var projects = [];
var authors = []
async function readProjects() {
    var pageUrlHead = 'http://cmmc.com.br/projetos/plo.php?pg=';
    var pageUrlTail = '&textfield_num=&textfield_assunto=&textfield_autor=&ano=';
    let pageParameter = 0;

    for (let i = intialYear; i < finalYear; i++) {
        while (await readProjectByYear(pageUrlHead + pageParameter + pageUrlTail + i, i)) {
            pageParameter++;
        }
        pageParameter = 0;
    }

    if (trace) {
        console.log(['projects length', projects.length]);
        console.log(['authors length', authors.length]);
    }
}

async function readProjectByYear(URL, year) {
    var response = await request({ uri: URL, encoding: 'latin1' });
    var $ = cheerio.load(response);

    var fields = $('td[align=center]');
    if (fields.length < 5)
        return false;

    for (let i = 4; i < fields.length; i += 4) {
        let aProjectRows = [];
        aProjectRows.push($(fields[i]).text());
        aProjectRows.push($(fields[i + 2]).text());
        aProjectRows.push($(fields[i + 3]).text());
        aProjectRows.push(year);
        aProjectRows.push($(fields[i]).find('a').attr('href'));

        let author = {
            projectId: $(fields[i]).text(),
            authors: []
        };

        $(fields[i + 1]).text().split(/ , | E /).forEach((nameAuthor) => {
            if (nameAuthor == 'PREFEITO')
                author.authors.push(prefeitos.findByYear(year));
            else author.authors.push(nameAuthor);
        });

        authors.push(author);
        projects.push(aProjectRows);
    }

    return true;
}

var projectsAuthorsRelationship = [];
function mapProjetAuthorRelationship() {
    return new Promise((resolve, reject) => {
        var sql = 'SELECT * FROM autor';
        dataAccessObject.getConnection().query(sql, function (err, results) {
            if (err)
                return reject(err);

            authors.forEach(item => {
                item.authors.forEach(authorName => {
                    let authorRegister = results.find(a => a.nome == authorName);
                    let relacionshipRows = [];
                    relacionshipRows.push(item.projectId);
                    relacionshipRows.push(authorRegister.id);
                    projectsAuthorsRelationship.push(relacionshipRows);
                });
            });

            if (trace) {
                console.log(['command', sql, 'rows returned', results.length]);
                console.log(['projectsAuthorsRelationship length', projectsAuthorsRelationship.length]);
            }

            resolve();
        });
    });
}

function bulkInsert(sql, values) {
    return new Promise((resolve, reject) => {
        dataAccessObject.getConnection().query(sql, [values], function (err, results) {
            if (err)
                return reject(err);

            if (trace)
                console.log(['command', sql, 'affected rows', results.affectedRows]);

            resolve();
        });
    });
}

//main
(async function () {
    readProjects()
        .then(dataAccessObject.connect)
        .then(mapProjetAuthorRelationship)
        .then(dataAccessObject.beginTran)
        .then(() => { return dropAllData.foreingKeyIO(dataAccessObject, false); })
        .then(() => { return dropAllData.deleteFrom(dataAccessObject, projectTableName); })
        .then(() => { return dropAllData.deleteFrom(dataAccessObject, relationshipTableName); })
        .then(() => { return dropAllData.foreingKeyIO(dataAccessObject, true); })
        .then(() => { return bulkInsert('INSERT INTO projeto_lei (numeroProjeto, assunto, anotacao, ano, link ) VALUES ?', projects); })
        .then(() => { return bulkInsert('INSERT INTO projeto_lei_x_autor (numeroProjetoLei, idAutor) VALUES ?', projectsAuthorsRelationship); })
        .then(dataAccessObject.commit)
        .then(dataAccessObject.endConnection)
        .catch((err) => {
            console.log(err);
            dataAccessObject.rollback().then(dataAccessObject.endConnection);
        });
})();